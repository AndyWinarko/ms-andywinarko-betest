import { Schema, model } from 'mongoose';
import bcrypt from 'bcrypt';
import crypto from 'crypto';

const saltRounds = 10;

const userSchema = new Schema({
    id: {
        type: String,
        required: false,
        unique: true
    },
    userName: {
        type: String,
        required: true,
        unique: false
    },
    accountNumber: {
        type: String,
        required: false,
        unique: true
    },
    emailAddress: {
        type: String,
        required: true,
        unique: true
    },
    identityNumber: {
        type: String,
        required: false
    },
    password: {
        type: String,
        trim: true,
        required: true
    }
});

userSchema.pre('save', function(next) {
    this.id = crypto.randomUUID();
    this.password = bcrypt.hashSync(this.password, saltRounds);
    next();
});

const User = model('User', userSchema);

export default User;