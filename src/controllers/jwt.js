import UserModel from '../model/user.js';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';

const register = async (req, res, next) => {

    const user = new UserModel({
        userName: req.body.userName,
        emailAddress: req.body.emailAddress,
        accountNumber: req.body.accountNumber,
        password: req.body.password
    });
    
    await user.save().then(data => {
        res.send({
            message:"User created successfully!!",
            user:data
        });
    }).catch(err => {
        next(err);
    });
}

const authenticate = async (req, res, next) => {
    try {
        const user = await UserModel.findOne({emailAddress: req.body.emailAddress});
        console.log(user);
        if (user != null && bcrypt.compareSync(req.body.password, user.password)) {
            const token = jwt.sign({id: user._id}, req.app.get('secretKey'), {expiresIn: '1h'});

            res.json({
                status: "success",
                message: "User found!",
                data: {
                    user: user,
                    token: token
                }
            });
        } else {
            res.json({
                status: "error",
                message: "Invalid email/password!",
                data: null
            });
        }

        res.status(200).json(user);
    } catch(err) {
        next(err);
    }
};

export default { register, authenticate };