import UserModel from '../model/user.js';
import redisClient from '../redis-client.js';

// Create and Save a new user
const create = async (req, res) => {
    if (!req.body.id && !req.body.userName && !req.body.accountNumber && !req.body.emailAddress && !req.body.identityNumber) {
        res.status(400).send({ message: "Content can not be empty!" });
    }
    
    const user = new UserModel({
        id: req.body.id,
        userName: req.body.userName,
        accountNumber: req.body.accountNumber,
        emailAddress: req.body.emailAddress,
        identityNumber: req.body.identityNumber,
        password: req.body.password
    });
    
    await user.save().then(data => {
        redisClient.set(`user:${req.body.id}`, JSON.stringify(user));

        res.send({
            message:"User created successfully!!",
            user:data
        });
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating user"
        });
    });
};

// Retrieve all users from the database.
const findAll = async (req, res) => {
    try {
        const cacheData = await redisClient.get('users');

        if (cacheData) {
            console.log('Data from cache');
            res.status(200).json(JSON.parse(cacheData));
            return;
        }

        console.log('Data from database');
        const user = await UserModel.find();

        if (!user.length) {
            res.status(404).json({ message: "No user found." });
            return;
        }

        await redisClient.setEx('users', 900, JSON.stringify(user));

        res.status(200).json(user);
    } catch(error) {
        res.status(404).json({message: error.message});
    }
};

// Find a single User with an id
const findOne = async (req, res) => {
    try {
        const cacheData = await redisClient.get(`user:${req.params.id}`);

        if (cacheData) {
            console.log('Data from cache');
            res.status(200).json(JSON.parse(cacheData));
            return;
        }

        console.log('Data from database');

        const user = await UserModel.findById(req.params.id);
        redisClient.set(`user:${req.params.id}`, JSON.stringify(user));

        if (!user) {
            res.status(404).json({ message: "User not found." });
            return;
        }

        res.status(200).json(user);
    } catch(error) {
        res.status(404).json({ message: error.message});
    }
};

// Update a user by the id in the request
const update = async (req, res) => {
    if(!req.body) {
        res.status(400).send({
            message: "Data to update can not be empty!"
        });
    }
    
    const id = req.params.id;
    
    await UserModel.findByIdAndUpdate(id, req.body, { useFindAndModify: false }).then(data => {
        if (!data) {
            res.status(404).send({
                message: `User not found.`
            });
        } else {
            redisClient.set(`user:${id}`, JSON.stringify(req.body));

            res.send({ message: "User updated successfully." })
        }
    }).catch(err => {
        res.status(500).send({
            message: err.message
        });
    });
};

// Delete a user with the specified id in the request
const destroy = async (req, res) => {
    await UserModel.findByIdAndDelete(req.params.id).then(data => {
        if (!data) {
            res.status(404).send({
                message: `User not found.`
            });
        } else {
            redisClient.del(`user:${req.params.id}`);
            res.send({
                message: "User deleted successfully!"
            });
        }
    }).catch(err => {
        res.status(500).send({
          message: err.message
        });
    });
};

export default { create, findAll, findOne, update, destroy };