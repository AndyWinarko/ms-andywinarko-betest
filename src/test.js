const request = require('supertest');
const app = require('./index'); // Assuming your Express app is exported from app.js
const UserModel = require('./model/user').default;

describe('User Controller', () => {
    let testUserId;

    // Create a new user
    it('should create a new user', async () => {
        const newUser = {
            userName: 'testUser',
            accountNumber: '1234567890',
            emailAddress: 'test@example.com',
            identityNumber: 'ABC123'
        };

        const response = await request(app)
            .post('/users')
            .send(newUser);

        expect(response.statusCode).toBe(200);
        expect(response.body).toHaveProperty('user');
        expect(response.body.user).toMatchObject(newUser);

        testUserId = response.body.user._id; // Save the user ID for later tests
    });

    // Get all users
    it('should get all users', async () => {
        const response = await request(app)
            .get('/users');

        expect(response.statusCode).toBe(200);
        expect(Array.isArray(response.body)).toBe(true);
    });

    // Get a single user
    it('should get a single user', async () => {
        const response = await request(app)
            .get(`/users/${testUserId}`);

        expect(response.statusCode).toBe(200);
        expect(response.body).toHaveProperty('_id');
        expect(response.body._id).toBe(testUserId);
    });

    // Update a user
    it('should update a user', async () => {
        const updatedUserData = {
            userName: 'updatedUser',
            accountNumber: '0987654321',
            emailAddress: 'updated@example.com',
            identityNumber: 'XYZ456'
        };

        const response = await request(app)
            .put(`/users/${testUserId}`)
            .send(updatedUserData);

        expect(response.statusCode).toBe(200);
        expect(response.body).toHaveProperty('message', 'User updated successfully.');
    });

    // Delete a user
    it('should delete a user', async () => {
        const response = await request(app)
            .delete(`/users/${testUserId}`);

        expect(response.statusCode).toBe(200);
        expect(response.body).toHaveProperty('message', 'User deleted successfully!');
    });
});
