import { Router } from 'express';
import userController from '../controllers/user.js';

const router = Router();

router.get('/', userController.findAll);
router.get('/:id', userController.findOne);
router.post('/', userController.create);
router.patch('/:id', userController.update);
router.delete('/:id', userController.destroy);

export default router