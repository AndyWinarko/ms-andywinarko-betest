import { Router } from 'express';
import jwt from '../controllers/jwt.js';

const router = Router();

router.post('/register', jwt.register);
router.post('/auth', jwt.authenticate);

export default router
