import express from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import jwt from 'jsonwebtoken';
import UserRoute from './routes/user.js';
import jwtAuthRoute from './routes/jwt.js';

const app = express();

let port = 3000;
if (process.env.PORT) {
    port = process.env.PORT;
}

const mongoUrl = `${process.env.DBHOST}/${process.env.DBNAME}`;

app.set('secretKey', 'ms_andywinarko_betest');

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json());

app.get('/', (req, res) => res.send('pong'));

mongoose.Promise = global.Promise;

function notFound(req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
}

function errors(err, req, res, next) {
    console.log(err);
    
    if (err.status === 404) {
        res.status(404).json({
            message: 'Not found'
        });
    } else {
        res.status(500).json({
            message: 'Internal Server Error'
        });
    }
}

function validateUser(req, res, next) {
    jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), (err, decoded) => {
        if (err) {
            res.json({
                status: "error",
                message: err.message,
                data: null
            });
        } else {
            req.body.userId = decoded.id;
            next();
        }
    });
}

async function main() {
    await mongoose.connect(mongoUrl, {
        useNewUrlParser: true
    });

    app.use('/', jwtAuthRoute);
    app.use('/user', validateUser, UserRoute);

    console.log("Database Connected Successfully!!");
    
    app.use(notFound);
    app.use(errors);
    
    app.listen(port, () => {
        console.log(`Server is listening on port ${port}`);
    });
}

await main()
    .then(() => console.log('microservice online'))
    .catch(err => {
        console.log('microservice failed to start');
        console.error(err && err.stack || err);
        process.exit();
    });
